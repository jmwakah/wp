<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<?php
	$default_sidebar_position = get_theme_mod( 'default_sidebar_position', 'right' );
	
		
					while ( have_posts() ) : the_post();
      $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
  <div class="container-fluid ">   <div class="col-md-12 "><!--<div class="single_shop_logo">
        <img src="  <?php //print $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" class=""/></div>--></div></div>
        <div class="container-fluid shop_list_svg1" style=" background: url(<?php print $url; ?>) no-repeat center center scroll; " >
        <div class="col-md-12 ">
            
        </div>
        
    </div> 

	 <div class="container home_body_1 single_shop_1">     
               <div class="row justify-content-centerx ">
        <div class="home_body_1_a single_shop_a single_post_cap col-md-8 ">
            <h3 class="pink">  <i class="fa fa-tags" aria-hidden="true"></i> In: <?php echo '<span>'. get_the_category( $id )[0]->name .'</span>';?> </h3> 
            <h3> <?php 
           the_title(); ?></h3>
            <p>                

         <?php echo  get_the_content();  ?>
            </p>
        </div>
               <div class="col-md-3 econcierge1_card2 econcierge1_card_single_post">
                   
                       <h3 class="text_center">In Brief </h3>
                       <div id="accordion">
   
                               <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapssed">
          <i class="fa fa fa-calendar" aria-hidden="true"></i> Published:   <?php echo '<span>'. $post_date = get_the_date( 'D M j' ).'</span>';?>        </button>
        </h5>
      </div>
    
    </div>
                                <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapssed">
          <i class="fa fa fa-archive" aria-hidden="true"></i> <?php global $post;
$category = get_the_category(); 
$parent = get_category($category[0]->category_parent);
print $parent->name;   ?>      </button>
        </h5>
      </div>
    
    </div>
                      
       
                                       <?php
                            if(get_field('date_from')) {?>
         <div class="card">
      <div class="card-header" id="headingTwo">
            <h5 class="mb-0">   <button class="btn btn-link collapssed">
                <a href="" target="_blank">   From: <?php echo  get_field('date_from') ;?></a><br>
                <br><a href="" target="_blank"> To: <?php echo  get_field('date_to') ;?></a>
                </button>
          </h5>
      </div>
    
    </div>
<?php } ?>
                                                 <?php
                            if(get_field('phone')) {?>
         <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapssed">
              <i class="fa fa-phone" aria-hidden="true"></i> <a href="" target="_blank"> <?php echo  get_field('phone') ;?></a>         </button>
        </h5>
      </div>
    
    </div>
<?php } ?>             <?php
                            if(get_field('shop_name')) {?>
         <div class="card">
      <div class="card-header" id="headingTwo">
        <h5 class="mb-0">
          <button class="btn btn-link collapssed">
          <i class="fa fa-cart" aria-hidden="true"></i> <a href="" target="_blank">    Shop: <?php echo  get_field('shop_name') ;?></a>         </button>
        </h5>
      </div>
    
    </div>
<?php } ?>
                                                   

                                                      
  </div>
        
                    </div>    
               
        
    </div>    
         
            
             </div> 

   
	<div class="container text_center single_shop_what_is">
          <div class="row justify-content-center"><div class="col-md-10 text_center">
                  <br>
                  <h3 class="text_center">         What’s happening </h3>
                  <br>
                  
              </div>  </div> 
			   <?php echo do_shortcode('[wcp-carousel id="141" order="DESC" orderby="date" count="10"]"'); ?>

      
   </div>
	
	<?php endwhile; // End of the loop.
					?>
	<!-- /.container -->
	<div class="container">
       
        
    </div>

<?php
get_footer();
