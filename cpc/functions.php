<?php
/**
 * WP Capital Center Functions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WP_Bootstrap_4
 */

if ( ! function_exists( 'wp_bootstrap_4_setup' ) ) :
	function wp_bootstrap_4_setup() {

		// Make theme available for translation.
		load_theme_textdomain( 'wp-bootstrap-4', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support( 'post-thumbnails' );

		// Enable Post formats
		add_theme_support( 'post-formats', array( 'gallery', 'video', 'audio', 'status', 'quote', 'link' ) );

		// Enable support for woocommerce.
		add_theme_support( 'woocommerce' );

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wp-bootstrap-4' ),
		) );

		// Switch default core markup for search form, comment form, and comments
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wp_bootstrap_4_custom_background_args', array(
			'default-color' => 'f8f9fa',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for core custom logo.
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wp_bootstrap_4_setup' );




/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wp_bootstrap_4_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wp_bootstrap_4_content_width', 800 );
}
add_action( 'after_setup_theme', 'wp_bootstrap_4_content_width', 0 );




/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wp_bootstrap_4_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wp-bootstrap-4' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget border-bottom %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 1', 'wp-bootstrap-4' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget wp-bp-footer-widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 2', 'wp-bootstrap-4' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget wp-bp-footer-widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 3', 'wp-bootstrap-4' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget wp-bp-footer-widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => esc_html__( 'Footer Column 4', 'wp-bootstrap-4' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'wp-bootstrap-4' ),
		'before_widget' => '<section id="%1$s" class="widget wp-bp-footer-widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h5 class="widget-title h6">',
		'after_title'   => '</h5>',
	) );
}
add_action( 'widgets_init', 'wp_bootstrap_4_widgets_init' );




/**
 * Enqueue scripts and styles.
 */
function wp_bootstrap_4_scripts() {
	wp_enqueue_style( 'open-iconic-bootstrap', get_template_directory_uri() . '/assets/css/open-iconic-bootstrap.css', array(), 'v4.0.0', 'all' );
	wp_enqueue_style( 'bootstrap-4', get_template_directory_uri() . '/assets/css/bootstrap.css', array(), 'v4.0.0', 'all' );
        wp_enqueue_style( 'capital_fa', get_template_directory_uri() . '/assets/font-awesome/css/font-awesome.min.css', array(), 'v4.0.0', 'all' );
       
	//wp_enqueue_style( 'wp-bootstrap-4-style', get_stylesheet_uri(), array(), '1.0.2', 'all' );

	wp_enqueue_script( 'bootstrap-4-js', get_template_directory_uri() . '/assets/js/bootstrap.js', array('jquery'), 'v4.0.0', true );  

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wp_bootstrap_4_scripts' );


/**
 * Registers an editor stylesheet for the theme.
 */
function wp_bootstrap_4_add_editor_styles() {
    add_editor_style( 'editor-style.css' );
}
add_action( 'admin_init', 'wp_bootstrap_4_add_editor_styles' );


// Implement the Custom Header feature.
require get_template_directory() . '/inc/custom-header.php';

// Implement the Custom Comment feature.
require get_template_directory() . '/inc/custom-comment.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Functions which enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

// Custom Navbar
require get_template_directory() . '/inc/custom-navbar.php';

// Customizer additions.
require get_template_directory() . '/inc/tgmpa/tgmpa-init.php';

// Use Kirki for customizer API
require get_template_directory() . '/inc/theme-options/add-settings.php';

// Customizer additions.
require get_template_directory() . '/inc/customizer.php';

// Load Jetpack compatibility file.
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Load WooCommerce compatibility file.
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

//new extension
/*
* Creating a function to create our CPT
*/
 
function custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Stores', 'Post Type General Name', 'capitalcenter' ),
        'singular_name'       => _x( 'Store', 'Post Type Singular Name', 'capitalcenter' ),
        'menu_name'           => __( 'Stores', 'capitalcenter' ),
        'parent_item_colon'   => __( 'Parent Store', 'capitalcenter' ),
        'all_items'           => __( 'All Stores', 'capitalcenter' ),
        'view_item'           => __( 'View Store', 'capitalcenter' ),
        'add_new_item'        => __( 'Add New Store', 'capitalcenter' ),
        'add_new'             => __( 'Add New', 'capitalcenter' ),
        'edit_item'           => __( 'Edit Store', 'capitalcenter' ),
        'update_item'         => __( 'Update Store', 'capitalcenter' ),
        'search_items'        => __( 'Search Store', 'capitalcenter' ),
        'not_found'           => __( 'Not Found', 'capitalcenter' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'capitalcenter' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'stores', 'capitalcenter' ),
        'description'         => __( 'Stores and their descriptions', 'capitalcenter' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
       
        'taxonomies'          => array( 'store_cat' ),
        
         //'taxonomies'          => array( 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        # 'rewrite'=> array( 'slug' => 'stores/%store%' )
    );
     
    // Registering your Custom Post Type
    register_post_type( 'stores', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'custom_post_type', 0 );

// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_service_taxonomies', 0 );

/**
 * Add a custom taxonomy
 *
 * Create one taxonomy, portfolio_cat for the post type "stores"
 */
function create_service_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Store Categories', 'taxonomy general name', 'capitalcenter' ),
		'singular_name'     => _x( 'Store Category', 'taxonomy singular name', 'capitalcenter' ),
		'search_items'      => __( 'Search Categories', 'capitalcenter' ),
		'all_items'         => __( 'All Categories', 'capitalcenter' ),
		'parent_item'       => __( 'Parent Category', 'capitalcenter' ),
		'parent_item_colon' => __( 'Parent Category:', 'capitalcenter' ),
		'edit_item'         => __( 'Edit Category', 'capitalcenter' ),
		'update_item'       => __( 'Update Category', 'capitalcenter' ),
		'add_new_item'      => __( 'Add New Category', 'capitalcenter' ),
		'new_item_name'     => __( 'New Category Name', 'capitalcenter' ),
		'menu_name'         => __( 'Categories', 'capitalcenter' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_in_nav_menus'	=> true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'store-category' ),
	);

	register_taxonomy( 'store-category', array( 'stores' ), $args );
}


add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'stores'); 
    $query->set('post_type',$post_type);
    return $query;
    }
}

//footer custom footer
function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'capital-footer-1' => __( 'Capital footer menu' ),
      'extra-menu' => __( 'Extra Menu' ),
     'footer_contacts' => __( 'Footer Contacts' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );







//for social media icons
// Social site icons for Quick Menu bar
function capitalcenter_add_customizer_sections( $wp_customize ) {
	$wp_customize->add_section( 'social_settings', array(
		'title'		=> __( 'Social Media Icons', 'wp-bootstrap-4' ),
		'priority'	=> 100,
	));
	
	$social_sites = capitalcenter_get_social_sites();
	$priority = 5;
	
	foreach( $social_sites as $social_site ) {		
		
		$wp_customize->add_setting( "$social_site", array(
		   	'type'				=> 'theme_mod',
		   	'capability'		=> 'edit_theme_options',
		   	'sanitize_callback'	=> 'esc_url_raw',
		));
		
		$wp_customize->add_control( $social_site, array(						
		   	'label'		=> ucwords( __( "$social_site URL:", 'social_icon' ) ),
		   	'section'	=> 'social_settings',
		   	'type'		=> 'text',
		   	'priority'	=> $priority,
		));
		
		$priority += 5;
	}
}
add_action( 'customize_register', 'capitalcenter_add_customizer_sections' );

// Social Media icon helper functions
function capitalcenter_get_social_sites() {	
	// Store social site names in array
	$social_sites = array(
            'facebook',
            'youtube',
            'twitter',
            'instagram'
	
				
	);
	return $social_sites;
}

// Get user input from the Customizer and output the linked social media icons
function capitalcenter_show_social_icons() {	
	$social_sites = capitalcenter_get_social_sites();
	
	// Any inputs that aren't empty are stored in $active_sites array
	foreach( $social_sites as $social_site ) {		
		if ( strlen( get_theme_mod( $social_site ) ) > 0 ) {
		   $active_sites[] = $social_site;
		}
	}
	
	// For each active social site, add it as a list item
	if ( !empty( $active_sites ) ) {
		
		echo '<ul class="futa_socs">';		
		foreach ( $active_sites as $active_site ) {
			
			echo "<li>";
			switch( $active_site ){
			case "phone":
					echo '<a href="'. get_theme_mod( $active_site ) .'"><i class="fa fa-2x fa-phone-square" aria-hidden="true"></i></a>';
				break;
			case "email":
					echo '<a href="'. get_theme_mod( $active_site ) .'"><i class="fa fa-2x fa-envelope-square" aria-hidden="true"></i></a>';
				break;
                            	case "skype":
					echo '<a href="'. get_theme_mod( $active_site ) .'"><i class="fa fa-2x fa fa-skype" aria-hidden="true"></i></a>';
				break;
                              	case "whatsapp":
					echo '<a href="'. get_theme_mod( $active_site ) .'"><i class="fa fa-2x fa fa-whatsapp" aria-hidden="true"></i></a>';
				break;
                            
				default:
					echo '<a href="'. esc_url( get_theme_mod( $active_site ) ) .'" target="_blank"><i class="fa fa-2x fa-'. $active_site .'" aria-hidden="true"></i></a>';
			}
			echo "</li>";
			
		}
		echo "</ul>";
	
	}
}


//select menu
class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth){
		$indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
	}
	function end_lvl(&$output, $depth){
		$indent = str_repeat("\t", $depth); // don't output children closing tag
	}
	/**
	* Start the element output.
	*
	* @param  string $output Passed by reference. Used to append additional content.
	* @param  object $item   Menu item data object.
	* @param  int $depth     Depth of menu item. May be used for padding.
	* @param  array $args    Additional strings.
	* @return void
	*/
	function start_el(&$output, $item, $depth, $args) {
 		$url = '#' !== $item->url ? $item->url : '';
 		$output .= '<option value="' . $url . '">' . $item->title;
	}	
	function end_el(&$output, $item, $depth){
		$output .= "</option>\n"; // replace closing </li> with the option tag
	}
}
function menu_as_dropdown( $atts ) {
	// Attributes
	extract( shortcode_atts(
		array(
			'menu' => 'Select Menu',
		), $atts )
	);
	ob_start();
		wp_nav_menu( array(
			// 'theme_location' => 'mobile',
			'menu'           => $menu,
			'walker'         => new Walker_Nav_Menu_Dropdown(),
			'items_wrap'     => '<div class="mobile-menu"><form><select onchange="if (this.value) window.location.href=this.value">%3$s</select></form></div>',
		) );
	return ob_get_clean();
}
add_shortcode( 'menudropdown', 'menu_as_dropdown' );

//cusstom paginationthe_posts_navigation
function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
        if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";

        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
            }
        }

        if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

/* for latest post shortcode*/
/*function my_recent_posts_shortcode($atts){
 $q = new WP_Query(
   array( 'orderby' => 'date', 'posts_per_page' => '1')
 );

$list = '<div class="row_soo">';

while($q->have_posts()) : $q->the_post();

 $list .= '<class="row_half">' . get_the_date() . '<a href="' . get_permalink() . '">' . get_the_title() . '</a>' . '<br />' . get_the_excerpt() . '</div>';
 $list .= '<class="row_half">' . get_the_date() . '<a href="' . get_permalink() . '">' . get_the_title() . '</a>' . '<br />' . get_the_excerpt() . '</div>';

endwhile;

wp_reset_query();

return $list . '</div>';


}

add_shortcode('recent-posts', 'my_recent_posts_shortcode');*/

