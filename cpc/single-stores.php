<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<?php
	$default_sidebar_position = get_theme_mod( 'default_sidebar_position', 'right' );
	
		
					while ( have_posts() ) : the_post();
 $imgID = get_sub_field('shop_main_image');
	$imgSize = "full"; // (thumbnail, medium, large, full or custom size)
	$imgArr = wp_get_attachment_image_src( $attachid, $size );
?>
  <div class="container-fluid ">   <div class="col-md-12 "><div class="single_shop_logo">
        <img src="  <?php print $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>" class=""/></div></div></div>
        <div class="container-fluid shop_list_svg1" style=" background: url(<?php the_field('shop_main_image'); ?>) no-repeat center center scroll; " >
  
        
    </div> 

	 <div class="container-fluid home_body_1 single_shop_1">  
<div class="container">	 
               <div class="row justify-content-centerx ">
        <div class="home_body_1_a single_shop_a col-md-7 ">
           
            <h3 style="text-transform:uppercase;"> <?php 
			 print $post->post_title;
            /*  if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } */?></h3>
            <p>                

         <?php echo  get_the_content();  ?>
            </p>
        </div>
               <div class="col-md-3 econcierge1_card2 card_single_shop">
                       <h3 class="text_center">Visit us </h3>
                      
    <?php
                            if(get_field('working_hours')) {?>
                               <div class=" card_working row">
      
                                 <div class="col-md-2">    <i class="fa fa fa-clock-o" aria-hidden="true"></i>  </div>
                                       <div class="col-md-10">  <?php the_field('working_hours'); ?>    </div>
      
      
      </div>
    
 
							<?php } 	
							     if(!get_field('working_hours')) {?>
                   
                                                 <div class=" card_working row">
      
                                 <div class="col-md-2">    <i class="fa fa fa-clock-o" aria-hidden="true"></i>  </div>
                                       <div class="col-md-10">     No hours available     </div>
      
      
      
    
    </div>
							<?php } ?><?php
                            if(get_field('telephone_number')) {?>
      <div class=" card_working row">
      
                                 <div class="col-md-2"> 
                                     <i class="fa fa-volume-control-phone" aria-hidden="true"></i> </div><div class="col-md-10"> <?php

if(get_field('telephone_number'))
{
	echo  get_field('telephone_number') ;
}

?>        </div>
       
      </div>
    

							<?php } ?>
                                            <?php
                            if(get_field('email_address')) {?>
      <div class=" card_working row">
      
                                 <div class="col-md-2"> 
                                     <i class="fa fa-envelope-open-o" aria-hidden="true"></i></div>
                                     <div class="col-md-10">  <?php echo  get_field('email_address') ;?>     </div>
      </div>
    

<?php } ?>
                           <?php
                            if(get_field('website')) {?>
      <div class=" card_working row">
      
                                 <div class="col-md-2"> 
                                     <i class="fa fa-globe" aria-hidden="true"></i> </div>
       <div class="col-md-10"> 
           <a href="<?php echo  get_field('website') ;?>" target="_blank"> Visit our website</a>  
        
      </div>
    
    </div>
<?php } ?>
                                  <?php
                            if(get_field('facebook')) {?>
         <div class=" card_working row">
      <div class="col-md-2"> 
                                     <i class="fa fa-facebook" aria-hidden="true"></i></div>
                                      <div class="col-md-10"> <a href="<?php echo  get_field('facebook') ;?>" target="_blank"> Find us on Facebook</a>        
      </div>
    
    </div>
<?php } ?>
                                        <?php
                            if(get_field('twitter')) {?>
         <div class=" card_working row">
      <div class="col-md-2"> 
          <i class="fa fa-twitter" aria-hidden="true"></i> </div>
          <div class="col-md-10"> <a href="<?php echo  get_field('twitter') ;?>" target="_blank"> Tweet Us</a> 
      </div>
    
    </div>
<?php } ?>
                                        <?php
                            if(get_field('instagram')) {?>
         <div class=" card_working row">
      <div class="col-md-2"> 
          <i class="fa fa-instagram" aria-hidden="true"></i> </div> <div class="col-md-10"> <a href="<?php echo  get_field('instagram') ;?>" target="_blank"> Follow Us on Instagram</a>       
      </div>
    
    </div>
<?php } ?>
                           
                          
                           
                    
     
 
                                                   

                                                      
  </div>
        
                    </div> 
</div>					
               
        
    </div>    
         
            
             </div> 
<div class="container">
     <div class="row  ">
   
              <?php

    $images = acf_photo_gallery('shop_image_gallery', $post->ID);    
    if( count($images) ):
    
        foreach($images as $image):
            $id = $image['id']; 
            $title = $image['title']; 
            $caption= $image['caption']; 
            $full_image_url= $image['full_image_url'];
          //$full_image_url = acf_photo_gallery_resize_image($full_image_url, 500, 400);              
            
           $thumbnail_image_url= $image['thumbnail_image_url']; 
            $url= $image['url']; 
            $target= $image['target']; 
            $alt = get_field('photo_gallery_alt', $id); 
            $class = get_field('photo_gallery_class', $id); 
?>
<div class="col-xs-6 col-md-6 shop_gallery" style=" background: url(<?php print $full_image_url; ?>) no-repeat center center scroll; ">
    <!--<div class="thumbnail " >
       
    </div>-->
  <!--   <div class="thumbnail shop_gallery">
        <?php if( !empty($url) ){ ?><a href="<?php echo $url; ?>" <?php echo ($target == 'true' )? 'target="_blank"': ''; ?>><?php } ?>
            <img src="<?php echo $full_image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
        <?php if( !empty($url) ){ ?></a><?php } ?>
    </div>-->
</div>
<?php endforeach; endif; ?>
         
         

                 
             </div>
    
</div>
    <!--2--> <div class="container-fluid home_body_2_svg shops_cards text_center">
	<div class="container">	
        <h3>Find Shops</h3>
        <div class="row">
                         
                                      <?php	

$titlelength = 50; 
$args=array (
	'post_type' => 'stores',
	'posts_per_page' => 8,
  'post__not_in' => array($currentID),
  'orderby' => 'rand'
      
    );
$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  //$readMore=get_post_meta($post->ID, 'read_more', true); 
 
?>  
		 <div class="col-md-3">
          <div class="shop_list_svg_img  " style=" background: url(<?php print $url;?>) no-repeat center center scroll; " data-link="<?php the_permalink(); ?>" >
                      
                  </div> 
                         <h4><a href="<?php the_permalink(); ?>"><?php 
						 print $post->post_title;
						 
            /*  if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); }*/ ?></a></h4> 
              
                  
              </div> 			
                                                   <?php     
endwhile;
wp_reset_postdata();
    ?>
                  <!--2-->
                   
                 
                 
                  <div class="col-md-12">
                      <br>
                      <p class="text_center">
                          <a href="<?php bloginfo('url') ?>/all-stores" class="btn view_more">See all stores</a>
                      </p>
                  </div>
                  
          </div>
		  </div>
        
    </div>
	<div class="container-fluid text_center single_shop_what_is">
	<div class="container">	
          <div class="row "><div class="col-md-12 text_center">
              
                  <h3 class="text_center">         What’s happening </h3>
                  
              </div>  </div> 
			   <?php echo do_shortcode('[wcp-carousel id="141" order="DESC" orderby="date" count="10"]"'); ?>

      
   </div>
   </div>
	
	<?php endwhile; // End of the loop.
					?>
	<!-- /.container -->
	<div class="container">
      <div class="row"><div class="col-md-12 text_center">
              <br>
                  <h3 class="text_center pink">         Latest Stories </h3>
                             <br>
              </div>  </div> 
        <div class="row justify-content-center">
		              <?php	

$titlelength = 300; 
$args=array (
	'post_type' => 'post',
	'posts_per_page' => 2,
	'category_name' => 'story'
      
    );
$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  //$readMore=get_post_meta($post->ID, 'read_more', true); 
 
?>
            <div class="col-md-6">
                   <div class="story_1" style=" background: url(<?php print $url;?>) no-repeat center center scroll; " data-link="<?php the_permalink(); ?>"></div>
                <h4 ><a href="<?php the_permalink(); ?>" class="pink"><?php 
				print $post->post_title;
				/*
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } */?></a></h4>
                <p>
                   <?php
                                                 // echo  get_the_content();  
 echo wp_trim_words( get_the_content(), 20, '...' );
                                                        ?></p>
            </div>  
                                            <?php     
endwhile;
wp_reset_postdata();
    ?>			
           
            
        </div>
        
    </div>

<?php
get_footer();
