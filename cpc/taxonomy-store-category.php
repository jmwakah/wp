<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<?php
	$default_sidebar_position = get_theme_mod( 'default_sidebar_position', 'right' );
?>

	<div class="container">
    <div class="row shops_min justify-content-between">
        <div class=" col-md-4">
           <form class="form-inline">
 
   <div class="form-group" style="display:inline !important;">

     <?php
  
     wp_nav_menu( array(
	// 'theme_location' => 'mobile',
	'menu'           => 'home_stores',
	'walker'         => new Walker_Nav_Menu_Dropdown(),
	'items_wrap'     => '<div class="capital-stores"><form><select onchange="if (this.value) window.location.href=this.value">%3$s</select></form></div>',
) );
     ?>
</div>

</form>
        </div>
       <div class=" col-md-8 ">

        <?php				
          $terms = get_the_terms( $post->ID, 'store-category' );
if ( !empty( $terms ) ){
    // get the first term
    $term = array_shift( $terms );
     $postCat_slug=$term->name;
}
          ?>  
         <h3 class=""><?php echo $postCat_slug;?></h3>
       </div>
               
        
    </div>         
	<div class="row  custom_rowx shops_list store_archivex  justify-content-around">
          <?php				
          $terms = get_the_terms( $post->ID, 'store-category' );
if ( !empty( $terms ) ){
    // get the first term
    $term = array_shift( $terms );
    $postCat_slug=$term->slug;
}
          ?>    
    <?php	

$titlelength = 50; 


$args = array(
 'post_type' => 'stores',
 'tax_query' => array(
 array(
 'taxonomy' => 'store-category',
 'field' => 'slug',
 'terms' => $postCat_slug
 ),
 ),
);

$the_query = new WP_Query($args);
if($the_query->have_posts()){
while ( $the_query->have_posts() ) {

	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 
?>  
		 <div class="col-md-3 shop_list_cards">
          <div class="all_shops_cards_img  " style=" background: url(<?php print $url;?>) no-repeat center center scroll; " data-link="<?php the_permalink(); ?>" >
                      
                  </div> 
                         <h3 class="text_center">	<a href="<?php the_permalink(); ?>"><?php 
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } ?></a></h3> 
              
                  
              </div> 			
                                                   <?php     
}
wp_reset_postdata();
}
else{
	print '<p class="text_center" ><br><h1 style="color:red">No Stores!!</h1><br></p>';
	
}
    ?>					  
              
                  
                  
                  
              </div> 
		<!-- /.row -->
	</div>
	<!-- /.container -->

<?php
get_footer();
