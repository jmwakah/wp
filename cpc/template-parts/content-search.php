<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WP_Bootstrap_4
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( ' mt-3r' ); ?>>
    <div class="row no_pad" style="">
    
	<div class="col-md-3 search_thumb">
		

		<?php 
              if ( has_post_thumbnail() ){
                   wp_bootstrap_4_post_thumbnail();     
              }
                else{
              print '<br><br><h4 style="text-align:left">No Image</h4>' ;       
               
                }
               
                
?>
            </div>
	<div class="col-md-9">
		  <header class="entry-header">
			<?php the_title( sprintf( '<h4class="entry-title " style="text-align:center; text-transform:uppercase;"><a href="%s" rel="bookmark" class="text-dark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta text-muted">
				<?php wp_bootstrap_4_posted_on(); ?>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header>
			<?php the_excerpt(); ?>
            <p style="text-align:center;" ><br><a class="btn btn-outline-dark btn-sm" href="<?php the_permalink(); ?>">Read More</a></p>

	</div>
	<!-- /.card-body -->

	<?php if ( 'post' === get_post_type() ) : ?>
		<!--<footer class="entry-footer card-footer text-muted">
			<?php //wp_bootstrap_4_entry_footer(); ?>
		</footer>--><!-- .entry-footer -->
	<?php endif; ?>
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
