<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_4
 */

get_header(); ?>

<?php
	$default_sidebar_position = get_theme_mod( 'default_sidebar_position', 'right' );
?>

	<div class="container">
		<div class="row">
<br>
			
				<div class="col-md-12 wp-bp-content-width">
			

				<div id="primary" class="content-area wp-bp-404" style="text-align:center">
					<main id="main" class="site-main">

								<section class="error-404 not-found">
									<header class="page-header">
										<h1 class="page-title">
                      <br>
			
			<?php esc_html_e( 'Sorry! The page you are looking for is not available. ', 'wp-bootstrap-4' ); ?></h1>
                         <br>
			
									</header><!-- .page-header -->
     <br>
			
									<div class="page-content">
								<a href="<?php bloginfo("url") ?>" class="btn btn-primary btn-lg">Go to Home </a>
                         <br>
			
					
								<br>

										<?php
											//get_search_form();

																				?>

									


									</div><!-- .page-content -->
								</section><!-- .error-404 -->
						
						<!-- /.card -->

					</main><!-- #main -->
				</div><!-- #primary -->
			</div>
			<!-- /.col-md-8 -->

		
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container -->
<?php
get_footer();
