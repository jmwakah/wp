<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_4
 */


if( is_page( array( 25,19,21 ,23,15,13) ) ) {
include 'page-templates/help.php';
}

?>
<?php include 'upper_footer.php';?>


	</div><!-- #content -->

	<footer id="colophon" class="site-footer text-center mt-4 footer_svg">

		<section class="footer-widgets text-left">
                    <div class="container">
		<div class="row">
			<div class="col-lg-6 footer_left">
        <div class="footer_left_top">
                          
          <?php
wp_nav_menu( array( 
    'theme_location' => 'capital-footer-1', 
    'container_class' => 'footer_top' ) ); 
?>
        </div>
                           
            <?php
wp_nav_menu( array( 
    'theme_location' => 'footer_contacts', 
    'container_class' => 'footer_contacts' ) ); 
?>
                        

			</div>
                    <div class="col-lg-6 footer_right text_right align_right">                        
                        <h4> Get To Know Us</h4>               
                     	<?php echo capitalcenter_show_social_icons(); ?>
                     <!-- <p>
                        <small>  Keep up to date with everything on Capital Centre</small>
                        <br>
                        <a data-toggle="modal" href="#subscribeModal"class="btn btn-lg footer_btn"><span style="color:black !important">SUBSCRIBE</span></a>
            <p>  -->      </div>
		</div>
    <hr>
    <div class="row bottom_footer">
           <div class="col-md-4">
               <img src="https://www.capitalcentre.co.ke/cms/wp-content/uploads/2018/09/capital_logo-2.png">
        </div>
        <div class="col-md-8">
         <p class="text_right footer_copy_right">Copyright &copy; Capital Centre 2018. All rights reserved. Developed by <a href="https://www.witstechnologies.co.ke/" target="_blank">Wits Technologies</a></p>
        </div>
    </div>
    
	</div>
			<!--<div class="container">
				<div class="row">
					<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-1-area mb-2">
								<?php dynamic_sidebar( 'footer-1' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-2-area mb-2">
								<?php dynamic_sidebar( 'footer-2' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-3-area mb-2">
								<?php dynamic_sidebar( 'footer-3' ); ?>
							</aside>
						</div>
					<?php endif; ?>

					<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
						<div class="col">
							<aside class="widget-area footer-4-area mb-2">
								<?php dynamic_sidebar( 'footer-4' ); ?>
							</aside>
						</div>
					<?php endif; ?>
				</div>
				
			</div>-->
		</section>

		<!--<div class="container">
			<div class="site-info">
				<a href="<?php echo esc_url( 'https://bootstrap-wp.com/' ); ?>"><?php esc_html_e( 'Bootstrap 4 WordPress Theme', 'wp-bootstrap-4' ); ?></a>
				<span class="sep"> | </span>
				<?php
					/* translators: 1: Theme name. */
					printf( esc_html__( 'Theme Name: %1$s.', 'wp-bootstrap-4' ), 'WP Bootstrap 4' );
				?>
			</div>
		</div>-->
		<!-- /.container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- The Modal -->
<div class="modal fade" id="subscribeModal">
  <div class="modal-dialog modal-sm  " >
    <div class="modal-content">     
      <div class="modal-header">
        <h4 class="modal-title">Enter Email</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">     
        <?php echo do_shortcode('[contact-form-7 id="330" title="subscribe"]'); ?>
      </div>  
    </div>
  </div>

    </div>
  <div class="modal fade" id="giftModal">
  <div class="modal-dialog modal-sm " >
    <div class="modal-content">     
      <div class="modal-header">
        <h4 class="modal-title">Get a gift card</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">     
        <?php echo do_shortcode('[contact-form-7 id="337" title="giftcard"]'); ?>
      </div>  
    </div>
  </div>  </div>
  
  
<script>
 jQuery(document).ready(function($){ 
	 if (screen.width < 767) {
	/*$(".about_click .dropdown-toggle::after").click(function(event){ 
       window.location.href  = "<?php bloginfo('url') ?>/about-us/"; 
       event.preventDefault();
 });
		$(".store_click").click(function(event){ 
       window.location.href  = "<?php bloginfo('url') ?>/all-stores/"; 
       event.preventDefault();
 });*/
}
  
  $(".categories_nav ul li:last").after('<li class="cat-item cat-item-44"><a href="<?php bloginfo("url") ?>/what-is-happening/">All</a></li>');
 //for side toggle
   $('#toggle-wrap,#toggle-wrap2').on('click', function() {
    $(this).toggleClass('active');
  
   // $('.subscribeToggle').animate({width: 'toggle'}, 200);
    $(".subscribeToggle").fadeToggle();
  });
  
  $(".fa_cloze").click(function(){
       $(".subscribeToggle").fadeOut("slow");
   });
 
   $("[data-link]").click(function() {
        window.location.href = $(this).attr("data-link");
        return false;
    });
   $("#to_blog").click(function(event){ 
       window.location.href  = "<?php bloginfo('url') ?>/maasai-market/"; 
       event.preventDefault();
 });
  //nav bar
   if ($(window).width() > 769) {
    $('.navbar .dropdown').hover(function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

    }, function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

    });

    $('.navbar .dropdown > a').click(function() {
      location.href = this.href;
    });

  }
	 
}) 
    
    </script>
<div class="mobile-footer">
	<div class="mobile-footer_1">
		<a href="mail:info@capitalcentre.co.ke" class="mob_mail"><i class="fa fa-envelope" aria-hidden="true"></i> <br>
		<span>Mail</span></a>
	</div>
	<div class="mobile-footer_1">
		<a href="tel:+254789316194"><i class="fa fa-volume-control-phone" aria-hidden="true"></i><br>
		<span>Call</span></a>
	</div>
		<div class="mobile-footer_1">
		<a  href="javascript:void(0);" id="toggle-wrap2" ><i class="fa fa-bookmark" aria-hidden="true"></i><br>
		<span>Subscribe</span></a>
	</div>
	<div class="mobile-footer_1">
		<a href="#giftModal" data-toggle="modal" class="mob_buuk"><i class="fa fa-gift" aria-hidden="true"></i> <br>
		<span>Gift Card</span></a>
	</div>
	<!--end mob stub-->
</div>

</body>
</html>
