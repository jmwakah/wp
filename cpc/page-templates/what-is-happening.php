<?php
/*
* Template Name: what is happening
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
			 
				  <div class="container what_is_happening">
               <div class="row what_is_happening_filter">
                   <div class="col-md-12 text_center"> <h3 class="">What's happening  </h3>   </div>
                     
                   <div class="col-md-12 categories_nav"><br>
        
                  
                         <ul>
    <?php  wp_list_categories( array(
        'orderby'    => 'name',
        'show_count' => false  ,
  'depth' => 1,
 'title_li' => ''
    ) ); ?> 
</ul>

                      
                   </div>
        
    </div>   
      
         <?php	
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    
global $post;
$postcat = get_the_category( $post->ID );
if ( ! empty( $postcat ) ) {
    //echo esc_html( $postcat[0]->slug );  
  $catSlug= $postcat[0]->slug;
}
              else{
   $catSlug= '';             
              }
        $args=array (
	'post_type' => 'post',
	'posts_per_page' => 8,	
     'paged' => $paged,
         'category_name' => $catSlug 
         );

$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  //$readMore=get_post_meta($post->ID, 'read_more', true); 
 
?> 
       <div class="row happening_wrapper">
                  
            <div class="col-md-3 happening_img2" style=" background: url(<?php print $url;?>) no-repeat center center scroll; " data-link="<?php the_permalink(); ?>">
                
            </div>
                   <div class="col-md-9 happening_txt">
                       <h4><?php
$categories = get_the_category();
 
if ( ! empty( $categories ) ) {
    echo esc_html( $categories[0]->name );   
}

?>					   </h4>
                         <h3><?php 
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } ?></h3><?php if($categories[0]->name==='offdsddder'){?>
		gfgffg
                      <p>
					  <?php         if(get_field('shop_name')) {?>
					 <i class="fa fa-map-marker" aria-hidden="true"></i> <?php print get_field('shop_name');?><br>
					  <?php }
					   if(get_field('date_from')) {?>
                         <i class="fa fa-clock-o" aria-hidden="true"></i> <?php print get_field('date_from');?>
					   <?php } ?>
                         </p>
		<?php } ?>
						 <p>
						<?php //print $categories[0]->name; ?> 
						 </p>
                         <p>
						 <?php
			  echo wp_trim_words( get_the_content(), 30, '...' );			 
						 ?>
						 
						
						 </p>
						 <p class="text_center">
				 <a href="<?php the_permalink(); ?>" class="btn btn-outline-dark">Read More + </a>		 
						 </p>
                    </div>
        
    </div>
	                                                  <?php     
endwhile;
           
             if (function_exists("pagination")) {
          pagination($the_query->max_num_pages);
      } 
          
//wp_reset_postdata();
         
    ?>	
      </div>
				

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();