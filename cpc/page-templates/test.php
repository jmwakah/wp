<?php
/*
* Template Name: what is happening
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
			 
				  <div class="container what_is_happening">
               <div class="row what_is_happening_filter">
                   <div class="col-md-12 text_center"> <h3 class="">What's happening  </h3>   </div>
                     
                   <div class="col-md-12"><br>
                      <!--  <div class="form-group has-search">
    <span class="fa fa-search form-control-feedback"></span>
    <input type="text" class="form-control" placeholder="Search">
  </div>-->
                       <br>
                       <a href="?" class="index__label___2snup">All</a> 
                       <a href="?category=offer" class="index__label___2snup">Offer</a> 
                     <a href="?category=event" class="index__label___2snup">Event</a> 
                     <a href="?category=story" class="index__label___2snup">Story</a>
                       <br>
                       <p>
                            <br>
                    <!--   <span class="large_txt"> 72</span> results-->
                       </p>
                   </div>
        
    </div>   
      
         <?php	
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$slug = isset($_GET['category']) ? $_GET['category'] : '';
$titlelength = 50; 
 if($slug!=''){           
$args=array (
	
	'posts_per_page' => 4,
   'orderby' => 'publish_date',
     'order' => 'DESC'
     

   );
 } else{
            $args=array (
	
	'posts_per_page' => 4,
	  'orderby' => 'publish_date',
     'order' => 'DESC'    
     
    );
 }
$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
  //$readMore=get_post_meta($post->ID, 'read_more', true); 
 
?> 
       <div class="row happening_wrapper">
                  
            <div class="col-md-3 happening_img2" style=" background: url(<?php print $url;?>) no-repeat center center scroll; ">
                
            </div>
                   <div class="col-md-9 happening_txt">
                       <h4><?php
$categories = get_the_category();
 
if ( ! empty( $categories ) ) {
    echo esc_html( $categories[0]->name );   
}

?>					   </h4>
                         <h3><?php 
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } ?></h3><?php if($categories[0]->name==='offdsddder'){?>
		gfgffg
                      <p>
					  <?php         if(get_field('shop_name')) {?>
					 <i class="fa fa-map-marker" aria-hidden="true"></i> <?php print get_field('shop_name');?><br>
					  <?php }
					   if(get_field('date_from')) {?>
                         <i class="fa fa-clock-o" aria-hidden="true"></i> <?php print get_field('date_from');?>
					   <?php } ?>
                         </p>
		<?php } ?>
						 <p>
						<?php //print $categories[0]->name; ?> 
						 </p>
                         <p>
						 <?php
			  echo wp_trim_words( get_the_content(), 50, '...' );			 
						 ?>
						 
						
						 </p>
						 <p class="text_center">
				 <a href="<?php the_permalink(); ?>" class="btn btn-outline-dark">Read More + </a>		 
						 </p>
                    </div>
        
    </div>
	                                                  <?php     
endwhile;
           
             if (function_exists("pagination")) {
          pagination($the_query->max_num_pages);
      } 
          
//wp_reset_postdata();
         
    ?>	
      </div>
				

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();