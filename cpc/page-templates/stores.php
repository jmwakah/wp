<?php
/*
* Template Name: Shops
*/

get_header(); ?>

    <div class="">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
			 <div class="container no_pad shop_listing">
               <div class="row  custom_rowx">
        <div class=" col-md-12 ">
           <form class="form-inline">
 
   <div class="form-group" style="display:inline !important;">

     <?php
  
     wp_nav_menu( array(
	// 'theme_location' => 'mobile',
	'menu'           => 'home_stores',
	'walker'         => new Walker_Nav_Menu_Dropdown(),
	'items_wrap'     => '<div class="capital-stores"><form><select onchange="if (this.value) window.location.href=this.value">%3$s</select></form></div>',
) );
     ?>
</div>

</form>
        </div>
               
        
    </div> 

			 <div class="row  custom_rowx justify-content-around">
              
    <?php	
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$titlelength = 50; 
$args=array (
	'post_type' => 'stores',
	'posts_per_page' => -1 ,
  'orderby' => 'publish_date',
  'order' => 'ASC'
	 
      
    );
$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 
 
?>  
		 <div class="col-md-3 shop_list_cards">
          <div class="all_shops_cards_img  " data-link="<?php the_permalink(); ?>" style=" background: url(<?php print $url;?>) no-repeat center center scroll; " >
                      
                  </div> 
                         <h3 class="text_center">	<a href="<?php the_permalink(); ?>"><?php 
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } ?></a></h3> 
              
                  
              </div> 			
                                                   <?php 
 											   
endwhile;

wp_reset_postdata();
    ?>					  
              
                  
                  
                  
              </div>  </div>
				
				

            </main><!-- #main -->
        </div><!-- #primary -->
    </div>

<?php
get_footer();