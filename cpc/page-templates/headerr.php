<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Shapely
 */

?>
<?php

$shapely_transparent_header         = get_theme_mod( 'shapely_transparent_header', 0 );
$shapely_transparent_header_opacity = get_theme_mod( 'shapely_sticky_header_transparency', 100 );

if ( 1 == $shapely_transparent_header && $shapely_transparent_header_opacity ) {
	if ( $shapely_transparent_header_opacity < 100 ) {
		$style = 'style="background: rgba(255, 255, 255, 0.' . esc_attr( $shapely_transparent_header_opacity ) . ');"';
	} else {
		$style = 'style="background: rgba(255, 255, 255, ' . esc_attr( $shapely_transparent_header_opacity ) . ');"';
	}
} else {
	$style = '';
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
		       <link href="http://rentapot.co.ke/fit/fitviu.css" rel="stylesheet" >
		
<meta name="msvalidate.01" content=9007CC04DF07664E965B74FECFB3F6BA>
</head>

<body <?php body_class(); ?>>

<?php if ( is_front_page() || is_home() ) { ?>
<div id="page" class="site front-page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'shapely' ); ?></a>
<header id="masthead" class="site-header block block-inverse block-fill-height app-header app-header-en-us <?php echo get_theme_mod( 'mobile_menu_on_desktop', false ) ? ' mobile-menu' : ''; ?>" role="banner">
<?php } else {?>
<div id="page" class="site front-pagex">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'shapely' ); ?></a>
<!--<div class="block block-inverse block-fill-height app-header app-header-en-us header_2">-->
<header id="masthead" class="site-header block block-inverse block-fill-height app-header app-header-en-us header_2 <?php echo get_theme_mod( 'mobile_menu_on_desktop', false ) ? ' mobile-menu' : ''; ?>" role="banner">
<?php } ?>	<div class="nav-container">
			<nav <?php echo $style; ?> id="site-navigation" class="main-navigation navbar navbar-transparent navbar-fixed-top navbar-padded app-navbar p-t-md" role="navigation">
				<div class="container nav-bar">
					<div class="flex-row">
						<div class="module left site-title-container">
							<?php shapely_get_header_logo(); ?>
						</div>
						<div class="module widget-handle mobile-toggle right visible-sm visible-xs">
							<i class="fa fa-bars"></i>
						</div>
						<div class="module-group right">
							<div class="module left">
								<?php shapely_header_menu(); ?>
							</div>
							<!--end of menu module-->
							
						</div>
						<!--end of module group-->
					</div>
				</div>
			</nav><!-- #site-navigation -->
                        
		</div>
		<?php if ( is_front_page() || is_home() ) { ?>
    <div class=block-xs-middle>
<div class=container>
<div class=row>
<div class="col-sm-12 text-center" style="text-shadow: #000 2px 2px 15px; ">
<h1 class="block-title m-b-md"><strong>Workout anytime, anywhere</strong></h1>
<p>Get access to personal training, group classes, nutritionist and gym/studios. Get the app for iPhone and android.</p>
<ul class="list-badges text-center m-t">
<li class="m-b ios-app-button">
<a href="#" rel=nofollow target=_blank onclick="ga('send', 'event', 'appdownloads', 'iOS');">
<img src="http://fitcms.rentapot.co.ke/wp-content/themes/shapely/img/en-5eb1a238.svg" alt="Fitviu for iPhone" title="Fitviu for iPhone" style="height: 50px;">
</a>
</li>
<li class="m-b android-app-button">
<a href="#" rel=nofollow target=_blank onclick="ga('send', 'event', 'appdownloads', 'Android');">
<img src="http://fitcms.rentapot.co.ke/wp-content/themes/shapely/img/en-d3afe83a.svg" alt="Fitviu for Android" title="Fitviu for Android" style="height: 75px;">
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
		<?php } else{ ?>
		<div class="block-xs-middle">
    <div class="container">
      <div class="row">
      <!--<span class="arrow"></span>-->
        <div class="col-sm-10 col-md-4">
          <div class="panel panel-default">
            <div class="panel-body text_center">
              <p class="lead" style="    color: #1fbb9f;">
  <strong>Signup as a Fitviu provider below</strong>
</p>
<form>
<!---->   <!-- <p>
      If you have multiple cars and trainers,
      <a href="https://fleets.Fitviu.eu/signup?lang=en-us&amp;utm_source=driver-portal&amp;utm_medium=homepage">signup as a fleet owner here</a>.
    </p>-->
	<div id="eeee" class="form-group form-group-lg ember-view">    <label for="email" class="control-label">Names</label>
    <input id="names" name="names" required="" placeholder="John Doe" class="form-control ember-text-field ember-view" type="text">
    
    <div id="ember9" class="ember-view"><!----></div>

</div>
<div id="ember7" class="form-group form-group-lg ember-view">    <label for="email" class="control-label">Email</label>
    <input id="email" name="email" required="" placeholder="john.doe@gmail.com" class="form-control ember-text-field ember-view" type="email">
    <span class="help-block">This will be your username.</span>
    <div id="ember9" class="ember-view"><!----></div>

</div>
<div id="ember7" class="form-group form-group-lg ember-view">    <label for="email" class="control-label">Phone</label>
    <input id="email" name="phone" required="" placeholder="Phone" class="form-control ember-text-field ember-view" type="email">
   

</div>


<div class="form-group form-group-lg ember-view">
  <label for="sel1">City:</label>
  <select class="form-control" id="sel1">
  <option value="1">Select your City</option>
										<option value="Baragoi">Baragoi</option>
										<option value="Bungoma">Bungoma</option>
										<option value="Busia">Busia</option>
										<option value="Butere">Butere</option>
										<option value="Dadaab">Dadaab</option>
										<option value="Diani Beach">Diani Beach</option>
										<option value="Eldoret">Eldoret</option>
										<option value="Emali">Emali</option>
										<option value="Embu">Embu</option>
										<option value="Garissa">Garissa</option>
										<option value="Gede">Gede</option>
										<option value="Hola">Hola</option>
										<option value="Homa Bay">Homa Bay</option>
										<option value="Isiolo">Isiolo</option>
										<option value="Kitui">Kitui</option>
										<option value="Kibwezi">Kibwezi</option>
										<option value="Makindu">Makindu</option>
										<option value="Wote">Wote</option>
										<option value="Mutomo">Mutomo</option>
										<option value="Kajiado">Kajiado</option>
										<option value="Kakamega">Kakamega</option>
										<option value="Kakuma">Kakuma</option>
										<option value="Kapenguria">Kapenguria</option>
										<option value="Kericho">Kericho</option>
										<option value="Keroka">Keroka</option>
										<option value="Kiambu">Kiambu</option>
										<option value="Kilifi">Kilifi</option>
										<option value="Kisii">Kisii</option>
										<option value="Kisumu">Kisumu</option>
										<option value="Kitale">Kitale</option>
										<option value="Lamu">Lamu</option>
										<option value="Langata">Langata</option>
										<option value="Litein">Litein</option>
										<option value="Lodwar">Lodwar</option>
										<option value="Lokichoggio">Lokichoggio</option>
										<option value="Londiani">Londiani</option>
										<option value="Loyangalani">Loyangalani</option>
										<option value="Machakos">Machakos</option>
										<option value="Malindi">Malindi</option>
										<option value="Mandera">Mandera</option>
										<option value="Maralal">Maralal</option>
										<option value="Marsabit">Marsabit</option>
										<option value="Meru">Meru</option>
										<option value="Mombasa">Mombasa</option>
										<option value="Moyale">Moyale</option>
										<option value="Mumias">Mumias</option>
										<option value="Muranga">Muranga</option>
										<option value="Nairobi">Nairobi</option>
										<option value="Naivasha">Naivasha</option>
										<option value="Nakuru">Nakuru</option>
										<option value="Namanga">Namanga</option>
										<option value="Nanyuki">Nanyuki</option>
										<option value="Naro Moru">Naro Moru</option>
										<option value="Narok">Narok</option>
										<option value="Nyahururu">Nyahururu</option>
										<option value="Nyeri">Nyeri</option>
										<option value="Ruiru">Ruiru</option>
										<option value="Shimoni">Shimoni</option>
										<option value="Takaungu">Takaungu</option>
										<option value="Thika">Thika</option>
										<option value="Vihiga">Vihiga</option>
										<option value="Voi">Voi</option>
										<option value="Wajir">Wajir</option>
										<option value="Watamu">Watamu</option>
										<option value="Webuye">Webuye</option>
										<option value="Wundanyi">Wundanyi</option>
  </select>
</div>
<div class="form-group form-group-lg ember-view">

  <label for="sel1">Provider category :</label>
  <label class="radio-inline">
      <input type="radio" name="provider_cat" value="trainer"  >trainer
    </label>
    <label class="radio-inline">
      <input type="radio" name="provider_cat" value="nutritionist">nutritionist
    </label>
    <label class="radio-inline">
      <input type="radio" name="provider_cat" value="gym_studio" >gym/studio
    </label></div>
	<div class="form-group form-group-lg ember-view">
<div id="divDailySelect" style="display:none;">
  <label for="sel1">Are you certified?  :</label>
  <label class="radio-inline">
      <input type="radio" name="yes" checked>Yes
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">No
    </label>
    </div>  </div>


<!----><div id="ember454" class="form-group form-group-lg ember-view"><button id="ember455" class="btn btn-success btn-lg btn-block ember-view btn_submit" type="submit">      Submit
 →</button>
</div>    <label class="text-muted text-smallest text-normal" style="color:gray;">
      By signing up, you accept our <a data-test-link="terms" target="_blank" href="#">Terms & Conditions </a> .
    </label>
</form>
            </div>
          </div>
        </div>
            <div class="col-sm-10 col-md-6 col-md-offset-1 p-b-md trainer_right">
          <h1 class="m-b-sm p-t-lg text-brightest bigger_h1">
            <strong>Become a provider at Fitviu</strong>
          </h1>
          <h5 class="lead text-brightest" style="text-align:center">
            Earn good money
            <strong>with your skills.</strong>
          </h5>
          <span class="arrow"></span>
        
        </div>
      </div>
      <div class="row">
        <div class="text-center text-success m-t-xlg">
          <a href="#" data-ember-action="" data-ember-action-456="456">
            <span class="icon icon-chevron-thin-down" style="font-size: 2.5em;"></span>
          </a>
        </div>
      </div>
    </div>
  </div>
		
		<?php } ?>
	</header><!-- #masthead -->
	<div id="content" class="main-container">
		<?php if ( ! is_page_template( 'page-templates/template-home.php' ) && ! is_404() && ! is_page_template( 'page-templates/template-widget.php' ) ) : ?>
			<div class="header-callout">
				<?php shapely_top_callout(); ?>
			</div>
		<?php endif; ?>

		<section class="content-area <?php echo ( get_theme_mod( 'top_callout', true ) ) ? '' : ' pt0 '; ?>">
			<div id="main" class="<?php echo ( ! is_page_template( 'page-templates/template-home.php' ) && ! is_page_template( 'page-templates/template-widget.php' ) ) ? 'container-fluid' : ''; ?>" role="main">
