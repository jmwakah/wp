<!--4-->
<?php if(is_page( array( 9 ) ) ) { ?>
    <div class="container-fluid home_body_2_svg shops_cards text_center">
        <h3>Find Shops</h3>
        <div class=" container">
          <div class="row">
                         
                                      <?php	

$titlelength = 50; 
$args=array (
	'post_type' => 'stores',
	'posts_per_page' => 8,
	'orderby' => 'publish_date',
    'order' => 'ASC',
      
    );
$the_query = new WP_Query($args);
while ( $the_query->have_posts() ) :
	$the_query->the_post(); 
	     $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
 
?>  
		 <div class="col-md-3">
          <div class="shop_list_svg_img  " style=" background: url(<?php print $url;?>) no-repeat center center scroll; " data-link="<?php the_permalink(); ?>">
                      
                  </div> 
                         <h4><a href="<?php the_permalink(); ?>"><?php 
              if (mb_strlen($post->post_title) > $titlelength)
			{ echo mb_substr(the_title($before = '', $after = '', FALSE), 0, $titlelength) . ' ...'; }
		else { the_title(); } ?></a></h4> 
              
                  
              </div> 			
                                                   <?php     
endwhile;
wp_reset_postdata();
    ?>
                  <!--2-->
                   
                 
                 
                  <div class="col-md-12">
                      <br>
                      <p class="text_center">
                          <a href="<?php bloginfo('url')?>/all-stores" class="btn view_more">See all stores</a>
                      </p>
                  </div>
                  
          </div>
        
    </div>    </div>
<?php } if(is_page( array( 9 ) )||is_singular( 'stores' )||is_single()){ ?>
      <!--5-->
    <div class="container-fluid home_body_4_svg shops_cards text_center">
        <h3>We are here to help</h3>
        <div class="row home_body_2_cards here_to_help" >
            <div class="col-md-4">
                <p class="text_center">
                    <i class="fa fa-user-md" ></i>
             
                </p>
            <h4><a href="<?php bloginfo('url')?>/concierge/">Concierge</a>

</h4>
            </div>
            <div class="col-md-4">
                <p class="text_center">
                    <i class="fa fa-cab" ></i>
                </p>
				<h4><a href="<?php bloginfo('url')?>/parking/">Parking</a>

            </h4>
            </div>
         
            <div class="col-md-4">
                <p class="text_center">
                    <i class="fa fa-diamond" ></i>
                </p>
				<h4><a href="<?php bloginfo('url')?>/getting-here/">Getting here</a>

</h4>
            </div>
        </div>
        
    </div>
	
	<?php } ?>